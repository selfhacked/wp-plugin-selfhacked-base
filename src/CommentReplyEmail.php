<?php

namespace Selfhacked\SelfhackedBase;

use Enpii\Wp\EnpiiBase\Wp;

/**
 * Class CommentReplyEmail
 * @package SelfHacked\SelfHackedNewspaper\Component\WpTheme
 */
class CommentReplyEmail {

	/**
	 * @var null
	 */
	public $text_domain = null;

	public function __construct( $text_domain ) {
		$this->text_domain = $text_domain;
		$this->initialize();
	}

	/**
	 *
	 */
	public function initialize() {
		add_action( 'admin_menu', [ $this, 'add_admin_menu' ] );
		add_action( 'admin_init', [ $this, 'settings_init' ] );

		add_action( 'wp_insert_comment', [ $this, 'comment_notification' ], 99, 2 );
		add_action( 'wp_set_comment_status', [ $this, 'comment_status_update' ], 99, 2 );

		add_filter( 'preprocess_comment', [ $this, 'verify_comment_meta_data' ] );

		add_filter( 'comment_form_default_fields', [ $this, 'comment_fields' ] );
		add_filter( 'comment_form_submit_field', [ $this, 'comment_fields_logged_in' ] );

		add_action( 'comment_post', [ $this, 'persist_subscription_opt_in' ] );

		add_action( 'init', [ $this, 'unsubscribe_route' ] );
	}

	/**
	 * @param int         $commentId
	 * @param \WP_Comment $comment
	 *
	 * @return bool
	 */
	public function comment_notification( $commentId, $comment ) {
		if ( $comment->comment_approved == 1 && $comment->comment_parent > 0 ) {
			$parent      = get_comment( $comment->comment_parent );
			$email       = $parent->comment_author_email;
			$unsubscribe = $this->get_unsubscribe_link( $parent );

			// prevent email from being sent to self
			if ( $email == $comment->comment_author_email ) {
				return false;
			}

			$subscription = get_comment_meta( $parent->comment_ID, 'subscribe_to_comment', true );

			if ( $subscription && $subscription == 'off' ) {
				return false;
			}

			$logo_id = get_option( 'options_comment_reply_email_logo', 0 );

			$title              = sprintf( __( 'Hi %s', $this->text_domain ), $parent->comment_author ) . ',';
			$description        = '<b>' . $comment->comment_author . '</b> ' . __( 'has replied to your comment on', $this->text_domain );
			$post_link          = get_permalink( $parent->comment_post_ID );
			$post_title         = get_the_title( $parent->comment_post_ID );
			$comment            = esc_html( $comment->comment_content );
			$reply_link         = get_comment_link( $parent->comment_ID );
			$reply_text         = __( 'Click here to reply', $this->text_domain );
			$unsubscribe_header = __( 'Want to stop receiving these messages?', $this->text_domain );
			$unsubscribe_link   = $this->get_unsubscribe_link( $parent );
			$unsubscribe_text   = __( 'Unsubscribe', $this->text_domain );

			$email_body = Wp::get_template_part( 'parts/comment_reply_email', [
				'logo_id'            => $logo_id,
				'title'              => $title,
				'description'        => $description,
				'post_link'          => $post_link,
				'post_title'         => $post_title,
				'comment'            => $comment,
				'reply_link'         => $reply_link,
				'reply_text'         => $reply_text,
				'unsubscribe_header' => $unsubscribe_header,
				'unsubscribe_link'   => $unsubscribe_link,
				'unsubscribe_text'   => $unsubscribe_text,
			] );

			$subject = html_entity_decode( get_option( 'blogname' ), ENT_QUOTES ) . ' - ' . __( 'New reply to your comment', $this->text_domain );

			add_filter( 'wp_mail_content_type', [ $this, 'wp_mail_content_type_filter' ] );

			wp_mail( $email, $subject, $email_body );

			remove_filter( 'wp_mail_content_type', [ $this, 'wp_mail_content_type_filter' ] );
		}
	}

	/**
	 * @param $contentType
	 *
	 * @return string
	 */
	public function wp_mail_content_type_filter( $contentType ) {
		return 'text/html';
	}

	/**
	 * @param $comment
	 *
	 * @return string
	 */
	private function get_unsubscribe_link( $comment ) {
		$key = $this->secret_key( $comment->comment_ID );

		$params = [
			'comment' => $comment->comment_ID,
			'key'     => $key
		];

		$uri = site_url() . '/comment-reply-email/unsubscribe?' . http_build_query( $params );

		return $uri;
	}

	/**
	 * @param $commentId
	 *
	 * @return false|string
	 */
	private function secret_key( $commentId ) {
		return hash_hmac( 'sha512', $commentId, wp_salt(), false );
	}

	/**
	 *
	 */
	public function unsubscribe_route() {
		$requestUri = $_SERVER['REQUEST_URI'];

		if ( preg_match( '/comment-reply-email\/unsubscribe/', $requestUri ) ) {
			$commentId = filter_input( INPUT_GET, 'comment', FILTER_SANITIZE_NUMBER_INT );
			$comment   = get_comment( $commentId );

			if ( ! $comment ) {
				_e( 'Invalid request.', $this->text_domain );
				exit;
			}

			$userKey = filter_input( INPUT_GET, 'key', FILTER_SANITIZE_STRING );
			$realKey = $this->secret_key( $commentId );

			if ( $userKey != $realKey ) {
				_e( 'Invalid request.', $this->text_domain );
				exit;
			}

			$uri = get_permalink( $comment->comment_post_ID );

			$this->persist_subscription_opt_out( $commentId );

			echo '<!doctype html><html><head><meta charset="utf-8"><title>' . get_bloginfo( 'name' ) . '</title></head><body>';
			echo '<p>' . __( 'Your subscription for this comment has been cancelled.', $this->text_domain ) . '</p>';
			echo '<script type="text/javascript">setTimeout(function() { window.location.href="' . $uri . '"; }, 3000);</script>';
			echo '</body></html>';
			exit;
		}
	}

	/**
	 * @param $commentId
	 * @param $commentStatus
	 */
	public function comment_status_update( $commentId, $commentStatus ) {
		/** @var \WP_Comment $comment */
		$comment = get_comment( $commentId );

		if ( $commentStatus == 'approve' ) {
			$this->comment_notification( $comment->comment_ID, $comment );
		}
	}

	/**
	 * @param $fields
	 *
	 * @return mixed
	 */
	public function comment_fields( $fields ) {
		$label   = apply_filters( 'comment_checkbox_label', __( 'Notify me of replies via email', $this->text_domain ) );
		$checked = $this->get_default_checked() ? 'checked' : '';

		$fields['subscribe_to_comment'] = '<div class="checkbox-wrapper">
                        <label class="checkbox-label">
                            ' . $label . '
                            <input id="subscribe_to_comment" name="subscribe_to_comment" type="checkbox" value="on" ' . $checked . '>
                            <span class="checkmark"></span>
                        </label>';

		if ( $this->display_gdpr_notice() ) {
			$fields['gdpr'] = $this->render_gdpr_notice();
		} else {
			$fields['subscribe_to_comment'] .= '</div>';
		}

		return $fields;
	}

	/**
	 * @param $submitField
	 *
	 * @return string
	 */
	public function comment_fields_logged_in( $submitField ) {
		$checkbox = '';

		if ( is_user_logged_in() ) {
			$label   = apply_filters( 'comment_checkbox_label', __( 'Notify me of replies via email', $this->text_domain ) );
			$checked = $this->get_default_checked() ? 'checked' : '';

			$checkbox = '<div class="checkbox-wrapper">
                        <label class="checkbox-label">
                            ' . $label . '
                            <input id="subscribe_to_comment" name="subscribe_to_comment" type="checkbox" value="on" ' . $checked . '>
                            <span class="checkmark"></span>
                        </label>';

			if ( $this->display_gdpr_notice() ) {
				$checkbox .= $this->render_gdpr_notice();
			} else {
				$checkbox .= '</div>';
			}
		}

		return $checkbox . $submitField;
	}

	/**
	 * @param $option
	 * @param $default
	 *
	 * @return mixed
	 */
	private function get_option( $option, $default ) {
		$options = get_option( 'cre_settings' );

		if ( $options && isset( $options[ $option ] ) ) {
			return $options[ $option ];
		}

		return $default;
	}

	/**
	 * @return mixed
	 */
	private function get_default_checked() {
		return $this->get_option( 'subscription_check_by_default', false );
	}

	/**
	 * @return mixed
	 */
	private function display_gdpr_notice() {
		return $this->get_option( 'display_gdpr_notice', false );
	}

	/**
	 * @return mixed
	 */
	private function get_privacy_policy_url() {
		return $this->get_option( 'privacy_policy_url', '' );
	}

	/**
	 * @return mixed
	 */
	private function get_terms_of_service_url() {
		return $this->get_option( 'terms_of_service_url', '' );
	}

	/**
	 * @return string
	 */
	private function render_gdpr_notice() {
		$terms_of_service_txt = __( 'Terms of Service', $this->text_domain );
		$terms_of_service_url = $this->get_terms_of_service_url();
		$terms_of_service     = '<a target="_blank" href="' . $terms_of_service_url . '">' . $terms_of_service_txt . '</a>';
		$privacy_policy_txt   = __( 'Privacy Policy', $this->text_domain );
		$privacy_policy_url   = $this->get_privacy_policy_url();
		$privacy_policy       = '<a target="_blank" href="' . $privacy_policy_url . '">' . $privacy_policy_txt . '</a>';
		$format               = __( 'I agree to %s\'s %s and %a', $this->text_domain );
		$label                = sprintf( $format, get_option( 'blogname' ), $terms_of_service, $privacy_policy );
		$label                = apply_filters( 'gdpr_checkbox_label', $label );

		return '<label class="checkbox-label">
                                     ' . $label . $privacy_policy . '
                            <input id="gdpr" name="gdpr" type="checkbox" value="yes" required="required">
                            <span class="checkmark"></span>
                        </label></div>';
	}

	/**
	 * @param $commentId
	 *
	 * @return bool|int
	 */
	public function persist_subscription_opt_in( $commentId ) {
		$value = ( isset( $_POST['subscribe_to_comment'] ) && $_POST['subscribe_to_comment'] == 'on' ) ? 'on' : 'off';

		return add_comment_meta( $commentId, 'subscribe_to_comment', $value, true );
	}

	/**
	 * @param $commentId
	 *
	 * @return bool|int
	 */
	private function persist_subscription_opt_out( $commentId ) {
		return update_comment_meta( $commentId, 'subscribe_to_comment', 'off' );
	}

	/**
	 * @param $comment
	 *
	 * @return mixed
	 */
	public function verify_comment_meta_data( $comment ) {
		if ( $this->display_gdpr_notice() && ! is_admin() ) {
			if ( ! isset( $_POST['gdpr'] ) ) {
				wp_die( __( 'Error: you must agree with the terms to send a comment. Hit the back button on your web browser and resubmit your comment if you agree with the terms.', $this->text_domain ) );
			}
		}

		return $comment;
	}

	/**
	 *
	 */
	public function add_admin_menu() {
		add_options_page(
			__( 'Comment Reply Email', $this->text_domain ),
			__( 'Comment Reply Email', $this->text_domain ),
			'manage_options',
			'comment_reply_email',
			[ $this, 'options_page' ]
		);
	}

	/**
	 *
	 */
	public function settings_init() {
		$defaults = [
			'subscription_check_by_default' => [
				'type'    => 'boolean',
				'default' => true
			],
			'display_gdpr_notice'           => [
				'type'    => 'boolean',
				'default' => false
			],
			'privacy_policy_url'            => [
				'type'    => 'string',
				'default' => ''
			],
			'terms_of_service_url'          => [
				'type'    => 'string',
				'default' => ''
			]
		];

		register_setting( 'cre_admin', 'cre_settings', $defaults );

		add_settings_section(
			'cre_admin_section',
			'',
			[ $this, 'settings_section_callback' ],
			'cre_admin'
		);

		add_settings_field(
			'subscription_check_by_default',
			__( 'Check the subscription checkbox by default', $this->text_domain ),
			[ $this, 'subscription_check_by_default_render' ],
			'cre_admin',
			'cre_admin_section'
		);

		add_settings_field(
			'display_gdpr_notice',
			__( 'Display the GDPR checkbox', $this->text_domain ),
			[ $this, 'display_gdpr_notice_render' ],
			'cre_admin',
			'cre_admin_section'
		);

		add_settings_field(
			'privacy_policy_url',
			__( 'Privacy Policy URL', $this->text_domain ),
			[ $this, 'privacy_policy_url_render' ],
			'cre_admin',
			'cre_admin_section'
		);

		add_settings_field(
			'terms_of_service_url',
			__( 'Terms of Service URL', $this->text_domain ),
			[ $this, 'terms_of_service_url_render' ],
			'cre_admin',
			'cre_admin_section'
		);
	}

	/**
	 *
	 */
	public function subscription_check_by_default_render() {
		$options = get_option( 'cre_settings' );
		echo '<input type="checkbox" name="cre_settings[subscription_check_by_default]" ' . checked( $options['subscription_check_by_default'], true, false ) . ' value="1">';
	}

	/**
	 *
	 */
	public function display_gdpr_notice_render() {
		$options = get_option( 'cre_settings' );
		echo '<input type="checkbox" name="cre_settings[display_gdpr_notice]" ' . checked( $options['display_gdpr_notice'], true, false ) . ' value="1">';
	}

	/**
	 *
	 */
	public function privacy_policy_url_render() {
		$options = get_option( 'cre_settings' );
		echo '<input type="text" name="cre_settings[privacy_policy_url]" value="' . $options['privacy_policy_url'] . '">';
	}

	/**
	 *
	 */
	public function terms_of_service_url_render() {
		$options = get_option( 'cre_settings' );
		echo '<input type="text" name="cre_settings[terms_of_service_url]" value="' . $options['terms_of_service_url'] . '">';
	}

	/**
	 *
	 */
	public function settings_section_callback() {
	}

	/**
	 *
	 */
	public function options_page() {
		echo '<form action="options.php" method="post"><h2>' . __( 'Comment Reply Email', $this->text_domain ) . '</h2>';
		settings_fields( 'cre_admin' );
		do_settings_sections( 'cre_admin' );
		submit_button();

		echo '</form>';
	}
}