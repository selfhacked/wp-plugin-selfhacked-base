<?php
/**
 * Plugin Name: Selfhacked Base
 * Description: The Base plugin for Selfhacked.com. Should be loaded as must use plugin.
 * Version: 18.11.14.00
 * Author: Selfhacked
 * Text Domain: selfhacked
 * Domain Path: /languages/
 */

class SelfhackedBase {
	static $text_domain = 'selfhacked';

	/**
	 * Fire initial things
	 */
	public static function initialize() {
		new \Selfhacked\SelfhackedBase\CommentReplyEmail( static::$text_domain );
		add_filter( 'option_active_plugins', [ self::class, 'filter_plugins_to_load' ] );
	}

	/**
	 * Filter plugins to load base on request
	 * @param $plugins
	 *
	 * @return array
	 */
	public static function filter_plugins_to_load( $plugins ) {
		global $request_uri;

		$action        = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] : '';
		$td_theme_name = isset( $_REQUEST['td_theme_name'] ) ? $_REQUEST['td_theme_name'] : '';

		// Only let `fantastic-elasticsearch` in plugin list on ajax search
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX && $td_theme_name == 'Newspaper' && ( 'td_ajax_search' === $action || 'td_ajax_block' === $action ) ) {
			$mandatory_plugins = [];

			$mandatory_plugins[] = 'fantastic-elasticsearch/elasticsearch.php';

			$filtered_plugins = [];
			foreach ( $plugins as $plugin ) {
				$k = array_search( $plugin, $mandatory_plugins );
				if ( false !== $k ) {
					$filtered_plugins[] = $plugin;
				}
			}

			return $filtered_plugins;
		}

		return $plugins;
	}
}

add_action( 'muplugins_loaded', [ 'SelfhackedBase', 'initialize' ] );